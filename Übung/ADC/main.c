/*
 * ADC-Demo.c
 *
 * Created: 23.12.2021 13:28:31
 * Author : magen
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include "lcd.h"
#include <util/delay.h>

#define U_REF 5.0
#define ADC_MAX_RES 1024.0

int main(void)
{
	//Das ADC-Wandlungsergebnis liegt zw. 0 - 1023, daher 2 Bytes (16 Bits)
	uint16_t result = 0;
	uint8_t buffer[32];
	float voltage;
	//Nachdem wir den analogen Eingang A0 verwenden, sind kene MUX-Bits zu setzen (Default=A0)
	//ADMUX |= (1<<REFS0); // | (1<<MUX0) | (1<<MUX1);
	
	//Wir wollen aber am A3 den analogen Eingang verwenden und m�ssen somit die MUX-Bits setzen
	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);
	
	//ADEN: ADC-Komponente aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	lcd_init(LCD_DISP_ON);
	
	
    /* Replace with your application code */
    while (1) {
		//Messung starten
		ADCSRA |= (1<<ADSC);
		
		//Feststellen, ob die Messung abgeschlossen ist, ist die Messung noch nicht abgeschlossen.
		//Erst wenn das Bit vom Mikro-C auf '0' gestzt ist, k�nnen wir auf das Ergebnis der Wandlung zugreifen!
		while(ADCSRA & (1<<ADSC));
		
		//Das Ergebnis
		result = ADCW;
		voltage = (ADCW/ADC_MAX_RES)*U_REF;
		//Ergebnis in einen String konvertieren
		sprintf(buffer, "V: %.2fV", voltage);
		lcd_clrscr();
		lcd_puts(buffer);
		_delay_ms(250);
    }
}

