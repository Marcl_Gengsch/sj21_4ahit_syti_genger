/*
 * ISR.c
 *
 * Created: 25.11.2021 14:10:37
 * Author : magen
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


int main(void) {
	cli(); //L�scht das Pin6 im SREG (Status Register => page 11 in data Sheet)
	DDRD |= (1<<DDD7);
	PORTD |= (1<<PORTD2) | (1<<PORTD3);
	
	//Konfiguration von INT0 und INT1
	EIMSK |= (1<<INT0) | (1<<INT1);
	
    sei(); //SREG das Pin7 setzt
    while (1){
		_delay_ms(1000);
	}
}

/************************************************************************/
/* ISR f�r PD2														    */
/* LED am PD7 einschalten                                               */
/************************************************************************/
ISR(INT0_vect) {
	PORTD |= (1<<PORTD7);
}

/************************************************************************/
/* ISR f�r PD3														    */
/* LED am PD7 ausschalten                                               */
/************************************************************************/
ISR(INT1_vect) {
	PORTD &= ~(1<<PORTD7);
}

