/*
 * main.c - Timer
 * LED am Pin PD2, die im Sekunden-Takt blinkt! Timer-basiert!
 *
 * Created: 24.02.2022 13:50:12
 * Author : Marcel
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>

int main(void) {
    
    //Pin PB3 als Ausgang konfiguriert
    DDRD |= (1<<DDD2);
    
    //Timer mit System-Takt (16MHz) aktiviert!
    TCCR1B = (1<<CS12);
    
    while (1) {
        //62500 damit wir genau eine Sekunde haben
        if(TCNT1 == 62500) {
        PORTD ^= (1<<PORTD2);
        }
    }
}
