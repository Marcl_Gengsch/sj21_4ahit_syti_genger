/*
 * LCD-Demo.c
 *
 * Created: 09.12.2021 13:32:16
 * Author : magen
 */ 
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include "../../LCD/lcd.h"
#include <string.h>

void RunningHelloWorld();

int main(void)
{
	// LCD initialisieren
	lcd_init(LCD_DISP_ON);
	
    /* Replace with your application code */
    while (1) {
		RunningHelloWorld();
    }
}

void RunningHelloWorld() {
	for(int i = 0; i < 16; i++) {
		for (int j = 0; j < i; j++) {
			lcd_putc(' ');
		}
		lcd_puts("Hello world");
		_delay_ms(500);
		lcd_clrscr();
		
	}
}
