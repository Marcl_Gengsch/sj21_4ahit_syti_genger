/*
 * LCD-Demo.c
 *
 * Created: 09.12.2021 13:32:16
 * Author : magen
 */ 
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include "../../LCD/lcd.h"


int main(void)
{
	// LCD initialisieren
	lcd_init(LCD_DISP_ON);
	
    /* Replace with your application code */
    while (1) {
		lcd_clrscr();
		lcd_puts("Du");
		_delay_ms(5000);
		lcd_puts(" stinkst");
	    _delay_ms(500);
    }
}

