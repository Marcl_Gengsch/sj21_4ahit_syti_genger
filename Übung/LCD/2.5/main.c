/*
 * Produktionsanlage2_5.c
 *
 * Created: 16.12.2021 13:40:46
 * Author : magen
 */ 

#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <string.h>
#include <stdio.h>

volatile uint8_t buffer[32];
volatile uint8_t cnt = 0;
volatile uint8_t running = 0;

int main(void) {
	lcd_init(LCD_DISP_ON);
	PORTD |= (1<<PORTD2) | (1<<PORTD3);
	EIMSK |= (1<<INT0) | (1<<INT1);
	cli();
	lcd_puts("zum Starten\n Taste dr�cken!");
	sei();
	while (1) {
		_delay_ms(1000);
		if (running)
		{	
			cnt = cnt + 1;
			sprintf(buffer, "Dauer: %u sek.", cnt);
			lcd_clrscr();
			lcd_puts(buffer);
		}
			
	}
	   
}

/************************************************************************/
/* ISR f?r PD2														    */
/************************************************************************/
ISR(INT0_vect) {
	running = 1;
}

/************************************************************************/
/* ISR f?r PD3														    */
/************************************************************************/
ISR(INT1_vect) {
	running = 0;
	lcd_clrscr();
	sprintf(buffer, "  Endzeit: \n %u sek", cnt);
	lcd_puts(buffer);
}
