/*
 * main.c - BWM
 * LED am Pin PD2, die im Sekunden-Takt blinkt! Timer-basiert!
 *
 * Created: 24.02.2022 13:50:12
 * Author : Marcel
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>

volatile uint16_t result = 0;
volatile uint8_t buffer[32];
volatile uint8_t intervall = 0;

int main(void) {
    
    //Pin PD2 als Ausgang konfiguriert
    DDRD |= (1<<DDD2);
    
    //Timer auf die 750ms vorladen
    TCNT1 = 65535 - (12142 * intervall);
    
    //Timer mit System-Takt (16MHz) aktiviert!
    TCCR1B = (1<<CS10) | (1<<CS12);
    
    //Timer Overflow Interrupt aktivieren
    TIMSK1 |= (1<<TOIE1);
    
    ADMUX |= (1<<REFS0) | (1<<MUX2);
	
	//ADEN: ADC-Komponente aktivieren
	//ADIE: ADC-Interrupt aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	sei();
	
	lcd_init(LCD_DISP_ON);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
    
    while (1);
}

ISR (ADC_vect) {
    //Das Ergebnis
	result = ADCW;
	
	//Ergebnis in einen String konvertieren
	sprintf(buffer, "ADCW: %u", result);
	
	lcd_clrscr();
	lcd_puts(buffer);
    
    if(result < 256)
        intervall = 1;
    else if (result < 512)
        intervall = 2;
    else if (result < 768)
        intervall = 3;
    else
        intervall = 4;
	
	_delay_ms(250);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
}

ISR (TIMER1_OVF_vect) {
    PORTD ^= (1<<PORTD2);
    TCNT1 = 65535 - (12142 * intervall);
}
