/*
 * Thermistor.c
 *
 * Created: 17.02.2022 14:14:56
 * Author : Marcel
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>
#include <math.h>
#include <stdio.h>

volatile double tempK = 0.00;
volatile double tempC = 0.00;
volatile char* buffer[32];


int main(void)
{

	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1);

	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADATE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);

	sei();
	
	lcd_init(LCD_DISP_ON);

	ADCSRA |= (1<<ADSC);
	while (1);
}

ISR(ADC_vect){
	double temp = 0;
	temp = ADCW;
	tempK = log(10000.0 * ((1024.0 / temp - 1)));
	tempK = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * tempK * tempK)) * tempK);
	tempC = tempK - 273.15;
	lcd_clrscr();
	sprintf(buffer, "Temp: %.2f", tempC);
	lcd_puts(buffer);
	_delay_ms(400);
}