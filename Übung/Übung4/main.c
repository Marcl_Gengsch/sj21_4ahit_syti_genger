/*
 * DigitalInput.c
 *
 * Created: 21.10.2021 14:09:15
 * Author : magen
 */ 
#define F_CPU 16000000 //16MHz

#include <avr/io.h>
#include <util/delay.h>

void pushButtonAndLED();
void switchAndLED();
void highEndRunningLED();
void pollButtons();
int d = 1;
void highEndRunningLEDAL();
void pollButtonsAL();

int main(void)
{
	DDRD = 0xff;
	PORTD = (1<<PORTD0);
	PORTC |= (1<<PORTC0) | (1<<PORTC1) | (1<<PORTC2);
    /* Replace with your application code */
    while (1) 
    {
		pollButtonsAL();
		highEndRunningLEDAL();
    }
}

void pushButtonAndLED(){
	if(PIND & (1<<PIND2)) 
		PORTD |= (1<<PORTD7);
	else
		PORTD &= ~(1<<PORTD7);
}

void switchAndLED(){
	if(PIND & (1<<PIND2))
		PORTD |= (1<<PORTD7);
	if(PIND & (1<<PIND2))
		PORTD &= ~(1<<PORTD7);
}

void highEndRunningLED(){
	for (int i = d;i > 0;i--){
		_delay_ms(100);
	}
	if (PORTD & (1<<PORTD7)){
		PORTD = (1<<PORTD0);
	}
	else{
		PORTD <<= 1;
	}
}

void pollButtons(){
	
	if(PINC & (1<<PINC0)){
		d = 10;
	}
	if(PINC & (1<<PINC1)){
		d = 20;
	}
	if(PINC & (1<<PINC2)){
		d = 30;
	}
}

void highEndRunningLEDAL(){
	for (int i = d;i > 0;i--){
		_delay_ms(100);
	}
	if (PORTD & (1<<PORTD7)){
		PORTD = (1<<PORTD0);
	}
	else{
		PORTD <<= 1;
	}
}

void pollButtonsAL(){
	
	if(!(PINC & (1<<PINC0))){
		d = 10;
	}
	if(!(PINC & (1<<PINC1))){
		d = 20;
	}
	if(!(PINC & (1<<PINC2))){
		d = 30;
	}
}