/*
 * joystickGame.c
 *
 * Created: 17.02.2022 10:22:12
 * Author : Marcel
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>

volatile uint8_t x = 7;
volatile uint8_t y = 0;

void print_position();

int main(void)
{
	//Nachdem wir den analogen Eingang A0 verwenden, sind kene MUX-Bits zu setzen (Default=A0)
	ADMUX |= (1<<REFS0) | (1<<MUX0) | (1<<MUX1); // | (1<<MUX0) | (1<<MUX1);
	//Joystick: X an A3 bzw. PC0 und Y an A4 bzw. PC1 anschließen
	
	//ADEN: ADC-Komponente aktivieren
	//ADIE: ADC-Interrupt aktivieren
	//ADATE: Auto Trigger Mode aktivieren -> Free Running Mode aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADATE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	// Free Running ist im ADCSRB zu "konfigurieren" => Default erledigt: ADTS0 - ADTS2 sind "0"!
	
	sei();
	
	lcd_init(LCD_DISP_ON);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
	while (1);
}

void print_position() {
	lcd_clrscr();
	lcd_gotoxy(x, y);
	lcd_putc('X');
}

ISR(ADC_vect){
	uint16_t temp = 0;
	//A4 (Y) ist der aktuelle Eingang
	if(ADMUX & (1<<MUX2)) {
		temp = ADCW;
		if(temp > 600) {
			y = 0;
		} 
		else if(temp < 300) {
			y = 1;
		}
		ADMUX &= ~(1<<MUX2);
		ADMUX |= (1<<MUX0) | (1<<MUX1);
	}
	//A3 (X) ist der aktuelle Eingang
	else {
		temp = ADCW;
		if(temp > 600 && x <= 14) {
			x--;
			} else if(temp < 500 && x > 0) {
			x++;
		}
		ADMUX |= (1<<MUX2);
		ADMUX &= ~((1<<MUX0) | (1<<MUX1));
	}
	print_position();
	_delay_ms(250);
}

