/*
 * main.c - Timer
 * LED am Pin PD2, die im Sekunden-Takt blinkt! Timer-basiert!
 *
 * Created: 24.02.2022 13:50:12
 * Author : Marcel
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

int main(void) {
    
    //Pin PD2 als Ausgang konfiguriert
    DDRD |= (1<<DDD2);
    
    //Timer auf die 1000ms vorladen
    TCNT1 = 3035;
    
    //Timer mit System-Takt (16MHz) aktiviert!
    TCCR1B = (1<<CS12);
    
    //Timer Overflow Interrupt aktivieren
    TIMSK |= (1<<TOIE1);
    
    //Interrupts global aktivieren
    sei();
    
    while (1);
}


ISR (TIMER1_OVF_vect) {
    PORTD ^= (1<<PORTD2);
    TCNT1 = 3035;
}
