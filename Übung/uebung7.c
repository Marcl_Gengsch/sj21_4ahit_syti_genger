/*
 * main.c - Timer
 * LED am Pin PD2, die im Sekunden-Takt blinkt! Timer-basiert!
 *
 * Created: 24.02.2022 13:50:12
 * Author : Marcel
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

int main(void) {
    DDRD |= (1<<DDD2);
    
    //Interrupt CTC Mode. gibts nur in B !
    // Prescaler
    TCCR1B |= (1<<WGM12) | (1<<CS10) | (1<<CS12);
    
    //Interrupt für CTC Mode Tim1 Kanal B
    TIMSK1 |= (1<<OCIE1A);
    
    //750ms
    OCR1A = 12142;
   
    ADMUX |= (1<<REFS0) | (1<<MUX2);
	
	//ADEN: ADC-Komponente aktivieren
	//ADIE: ADC-Interrupt aktivieren
	//ADPS0-2: Den Takt reduzieren auf 200kHz
	ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	
	sei();
	
	lcd_init(LCD_DISP_ON);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
    
    while (1);
}

ISR (ADC_vect) {
    //Das Ergebnis
	result = ADCW;
	
	//Ergebnis in einen String konvertieren
	sprintf(buffer, "ADCW: %u", result);
	
	lcd_clrscr();
	lcd_puts(buffer);
    
    TCCR1B &= (1<<CS10) | (1<CS12); 
    
    if(result < 256)
        OCR1A = 12142;
    else if (result < 512)
        OCR1A = 12142 * 2;
    else if (result < 768)
        OCR1A = 12142 * 3;
    else
        OCR1A = 12142 * 4;
	
    TCCR1B |= (1<<CS10) | (1<<CS12);
	
	//Messung starten
	ADCSRA |= (1<<ADSC);
}

ISR (TIMER1_COMPA_vect) {
    PORTD ^= (1<<PORTD2);
}