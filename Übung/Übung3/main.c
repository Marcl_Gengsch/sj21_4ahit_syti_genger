/*
 * HelloWorld.c
 *
 * Created: 23.09.2021 13:36:04
 * Author : magen
 */ 
#define F_CPU 16000000 //16MHz

#include <avr/io.h>
#include <util/delay.h>
#define delay() _delay_ms(500)

void lauflicht0();
void lauflicht1();
void lauflicht2();
void lauflicht3();
void lauflicht4();
void lauflicht5();
void lauflicht6();

int main(void) {
	DDRD = 0xFF;
	PORTD = 0x01;
	
    /* Replace with your application code */
    while (1) {
		lauflicht6();
	}
}

/************************************************************************/
/* Task 2.5                                                             */
/************************************************************************/

void lauflicht6() {
	PORTD <<= 1;
	delay();
	if (PORTD == 0x80) {
		for(int i = 0; i < 7; i++) {
			PORTD >>= 1;
			delay();
		}
	}
}

void lauflicht5() {
	_delay_ms(500);
	if (PORTD == 0x80) {
		PORTD = 0x01;
		delay();
	}
	PORTD <<= 1;
	
	
}

/************************************************************************/
/* Task 2.4 Schiebeoperatoren											*/
/************************************************************************/

void lauflicht4(){
	int DELAY = 500;
	PORTD |= (1<<PORTD0);
	_delay_ms(DELAY);
	PORTD &= ~(1<<PORTD0);

	PORTD |= (1<<PORTD1);
	_delay_ms(DELAY);
	PORTD &= ~(1<<PORTD1);

	PORTD |= (1<<PORTD2);
	_delay_ms(DELAY);
	PORTD &= ~(1<<PORTD2);

	PORTD |= (1<<PORTD3);
	_delay_ms(DELAY);
	PORTD &= ~(1<<PORTD3);
}

void lauflicht3() {
	int delay = 500;

	PORTD|=(1<<0);
	_delay_ms(delay);
	PORTD&=~(1<<0);

	PORTD|=(1<<1);
	_delay_ms(delay);
	PORTD&=~(1<<1);

	PORTD|=(1<<2);
	_delay_ms(delay);
	PORTD&=~(1<<2);

	PORTD|=(1<<3);
	_delay_ms(delay);
	PORTD&=~(1<<3);

}

/************************************************************************/
/* /**+ => STRG + ENTER                                                 */
/************************************************************************/

/************************************************************************/
/* Task 2.1 Bitmasken mit Tilde ~										*/
/************************************************************************/
void lauflicht2() {
	PORTD |= 0x01;
	_delay_ms(500);
	PORTD &= ~0x01;
	PORTD |= 0x02;
	_delay_ms(500);
	PORTD &= ~0x02;
	PORTD |= 0x04;
	_delay_ms(500);
	PORTD &= ~0x04;
	PORTD |= 0x08;
	_delay_ms(500);
	PORTD &= ~0x08;
}

/************************************************************************/
/* Task 2.1 Bitmasken													*/
/************************************************************************/
void lauflicht1(){
	PORTD |= 0x01;
	_delay_ms(500);
	PORTD &= 0xFE;
	PORTD |= 0x02;
	_delay_ms(500);
	PORTD &= 0xFD;
	PORTD |= 0x04;
	_delay_ms(500);
	PORTD &= 0xFB;
	PORTD |= 0x08;
	_delay_ms(500);
	PORTD &= 0xF7;
}

void lauflicht0(){
	PORTD =0xF1;
	_delay_ms(250);
	PORTD =0xF2;
	_delay_ms(250);
	PORTD =0xF4;
	_delay_ms(250);
	PORTD =0xF8;
	_delay_ms(250);
}

