/*
 * bewertetesUebungsbeispiel.c
 *
 * Created: 21.11.2021 21:14:30
 * Author : magen
 */ 

#include <avr/io.h>


#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>

void init();

int main() {
	init();
	while(1) {
		if(!(PINC & (1<<PINC2))) {	//active Low
			if(PORTD) {
				PORTD = 0x00;
			}
			else {
				PORTD |= (1<<PORTD0) | (1<<PORTD1);
			}
		}
		if(PORTD & ((1<<PORTD6) | (1<<PORTD7))) {
			_delay_ms(500);
			PORTD |= 0xFF;
			_delay_ms(1000);
			PORTD &= ~((1<<PORTD7) | (1<<PORTD6) | (1<<PORTD5) | (1<<PORTD4) | (1<<PORTD3) | (1<<PORTD2));
		}
		_delay_ms(500);
		PORTD = (PORTD<<2);
	}
}

void init() {
	DDRD |= 0xFF;
	DDRC &= ~(1<<DDC2);
	PORTD |= (1<<PORTD0) | (1<<PORTD1);
	PORTC |= (1<<PORTC2);
}


