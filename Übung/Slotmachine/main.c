/*
 * SlotMachine.c
 *
 * Created: 02.12.2021 13:27:50
 * Author : magen
 */ 
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void init();
uint8_t counter = 1;
uint8_t running;

int main(void)
{
	extern uint8_t counter;
	extern uint8_t running;
	/* Replace with your application code */
	init();
	while (1)
	{
		if(running)
		{
			for(int i = counter; i > 0; i--) {
				if(!running)
				break;
				_delay_ms(100);
			}
			if(!running)
			continue;
			PORTD = (PORTD<<1);
			if(PORTD == (1<<PORTD6)) {
				for(int i = 0; i < 6; i++) {
					for(int i = counter; i > 0; i--) {
						if(!running)
						break;
						_delay_ms(100);
					}
					if(!running)
					continue;
					PORTD = (PORTD>>1);
				}
			}
		}
	}
}

void init() {
	//LEDs
	PORTD |= (1<<PORTD0);
	DDRD |= (1<<DDD0) | (1<<DDD1) | (1<<DDD2) | (1<<DDD3) | (1<<DDD4) | (1<<DDD5) | (1<<DDD6);
	//clear Interrupts
	cli();
	//Internen PU-Widerstände aktivieren
	PORTC |= (1<<PORTC0) | (1<< PORTC1) | (1<<PORTC2);
	//Externe Interrupt-Gruppe für Port C aktivieren
	PCICR |= (1<<PCIE1);
	//Externe Interrupts auf Pin-Ebene aktivieren: PC0, PC1 und PC2
	PCMSK1 |= (1<<PCINT8) | (1<<PCINT9) | (1<<PCINT10);
	//set Interrupts
	sei();
}

ISR(PCINT1_vect){
	extern uint8_t counter;
	extern uint8_t running;
	//Prellen ist unter Umständen noch zu berücksichtigen!!!
	if (!(PINC & (1<<PINC0))) {
		//PORTD &= ~PORTD;
		//PORTD |= (1<<PORTD0);
		running = 1;
	}
	else if(!(PINC & (1<<PINC1))) {
		running = 0;
	}
	else if (!(PINC & (1<<PINC2))) {
		switch(counter) {
			case 1:
			counter++;
			break;
			case 2:
			counter++;
			break;
			case 3:
			counter = 1;
			break;
			default:
			counter = 1;
			break;
		}
	}
	_delay_ms(50);
}